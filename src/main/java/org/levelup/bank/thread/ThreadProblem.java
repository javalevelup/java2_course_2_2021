package org.levelup.bank.thread;

public class ThreadProblem {


    // t1: transfer(10, 15, 405);
    // t2: transfer(15, 10, 40);

    // t3: transfer(10, 10, 505); // synchronized - reentrant
    // t4: transfer(10, 10, 505);
    void transfer(Long accountIdFrom, Long accountIdTo, double amount) {
        Long a = accountIdFrom > accountIdTo ? accountIdTo : accountIdFrom;
        Long b = a == accountIdFrom ? accountIdTo : accountIdFrom;
        // deadlock
        synchronized (a) { // sync(10)
            synchronized (b) { // sync(10)
                // from - to
                // ...
            }
        }
    }

    public void firstBlock() {
        synchronized (this) { // t1
            secondBlock();
        }
    }

    public void secondBlock() {
        synchronized (this) { // берем блокировку еще раз
            System.out.println("second...");
        }
    }

}
