package org.levelup.bank.jdbc;

import org.levelup.bank.domain.ClientEntity;
import org.levelup.bank.jdbc.pool.PostreSqlConnectionManager;

import java.time.LocalDate;
import java.util.Collection;

public class BankApplication {

    public static void main(String[] args) {
        ConnectionManager cm = new PostreSqlConnectionManager();
        ClientRepository clientRepository = new JdbcClientRepository(cm);

        // clientRepository.createNewClient("Im", "Kim Chen", null, null);
        // clientRepository.printAllClients();


        Collection<ClientEntity> clients = clientRepository.findClientsWhenBirthdayBetween(
                LocalDate.of(1950, 1, 1),
                LocalDate.of(1980, 12, 31)
        );

        for (ClientEntity client : clients) {
            System.out.println(client.getClientId() + " " + client.getLastName() + " " + client.getBirthday());
        }


    }

}
