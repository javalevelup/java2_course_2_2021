package org.levelup.bank.jdbc.pool;

import org.levelup.bank.jdbc.ConnectionManager;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostreSqlConnectionManager implements ConnectionManager {

    // DriverManager
    // Driver
    // Connection

    // https://mvnrepository.com/artifact/org.projectlombok/lombok/1.18.20?

    private final ConnectionPool pool;

    public PostreSqlConnectionManager() {
        this.pool = new ConnectionPool();
    }

    @Override
    public Connection openConnection() throws SQLException {
        // JDBC URL
        // jdbc:<vendor_name/driver_name>://<dns_name/ip_address>:<port>/<database_schema_name>?<param1>=<value1>&<param2>=<value2>
        Connection connection = pool.acquireConnection();
        if (connection == null) {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/bank_application",
                    "postgres",
                    "root"
            );
            connection = proxyConnection(connection); // Создали прокси-объект
            pool.putConnectionToPool(connection);
        }
        return connection;
    }

    private Connection proxyConnection(Connection realConnection) {
        return (Connection) Proxy.newProxyInstance(
                realConnection.getClass().getClassLoader(),
                realConnection.getClass().getInterfaces(),
                new ConnectionCloseInvocationHandler(realConnection, pool)
        );
    }

}
