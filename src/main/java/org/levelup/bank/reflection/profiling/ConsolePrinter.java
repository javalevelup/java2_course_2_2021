package org.levelup.bank.reflection.profiling;

public class ConsolePrinter implements Printer {

    @Override
    @Profiling
    public void printInformation() {
        // получить текущее время - start
        System.out.println("Hello, I'm printing some information...");
        // посчитать разницу между текущем временем и временем начала метода - (end - start)
    }

    @Override
    public void withoutAnnotation() {
        System.out.println("Method without annotation was invoked");
    }

}
