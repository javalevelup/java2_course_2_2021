package org.levelup.bank.util;

import java.util.Random;

public final class StringUtils {

    private StringUtils() {}

    public static String generateString(int length) {
        Random r = new Random();
        // 65 - 90
        // 97 - 122
        String s = "";
        for (int i = 0; i < length; i++) {
            s += Character.toString((char) r.nextInt(122 - 97) + 97);
        }

        return s;
    }

}
