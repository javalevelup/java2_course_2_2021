package org.levelup.bank.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Collection;

@Data
@Entity
@ToString(exclude = "filials")
@Table(name = "manager")
@NoArgsConstructor
public class ManagerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "filial_manager",
            joinColumns = @JoinColumn(name = "manager_id"),          // колонка, которая находится в таблице filial_manager и имеет внешний ключ на текущую таблицу (на таблицу managers)
            inverseJoinColumns = @JoinColumn(name = "filial_id")     // колонка, которая находится в таблице filial_manager и имеет внешний ключ на связанную таблицу (на таблицу filials)
    )
    private Collection<FilialEntity> filials;

}
