package org.levelup.bank.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor // у entity всегда должен быть пустой конструктор (конструктор без параметров)
@Table(name = "accounts")
public class AccountEntity {

    @Id // первичный ключ
    @Column(name = "account_number")
    private String accountNumber;
    @Column(columnDefinition = "NUMERIC(19,0)")
    private double amount;          // double vs Double - double - если колонка имеет ограничение not null
//    @Column(name = "client_id")
//    private long clientId;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientEntity client;

}
